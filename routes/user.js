var express = require('express');
var router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const csrf = require('csurf');
const passport = require('passport');


const csrfProtection = csrf();
router.use(csrfProtection);


const authCheck = require('../middleware/check-auth');

// Models
const Users = require('../models/users.model');

// const user = new Users({
//     name: 'Andrew',
//     login: 'admin',
//     surname: 'Paniuk',
//     password: bcrypt.hashSync('2501', bcrypt.genSaltSync(10)),
//     email: 'andrzej.paniuk@gmail.com',
//     isAdmin: true
// });
// user.save();

router.get('/logout', authCheck.isLoggedIn, (req, res, next) => {
    req.logOut();
    res.redirect('/');
});

router.use('/', authCheck.notLoggedIn, (req, res, next) => {
    next();
});

router.get('/login', function(req, res, next) {
    const messages = req.flash('error');
    res.render('login', {
        title: '[Login Page]',
        csrfToken: req.csrfToken(),
        messages,
        hasErrors: messages.length > 0
    });
});

router.get('/registration', function(req, res, next) {
    const messages = req.flash('error');
    console.log(messages);
    res.render('registration', {
        title: '[Registration Page]',
        csrfToken: req.csrfToken(),
        messages,
        hasErrors: messages.length > 0
    });
});


router.post('/login', passport.authenticate('local.signin', {
    successRedirect: '/profile',
    failureRedirect: '/user/login',
    failureFlash: true
}));

router.post('/registration', passport.authenticate('local.signup', {
    successRedirect: '/profile',
    failureRedirect: '/user/registration',
    failureFlash: true
}));



module.exports = router;

// function isLoggedIn(req, res, next) {
//     if (req.isAuthenticated()) {
//         return next();
//     }
//     res.redirect('/');
// }
// function notLoggedIn(req, res, next) {
//     if (!req.isAuthenticated()) {
//         return next();
//     }
//     res.redirect('/');
// }