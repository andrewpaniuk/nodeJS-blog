var express = require('express');
var router = express.Router();
const multer = require('multer');
const path = require('path');
const util = require('util');
const formidable = require('formidable');
const fs = require('fs');

const authCheck = require('../middleware/check-auth');

// Models
const Posts = require('../models/posts.model');
const Comments = require('../models/comments.model');
const Users = require('../models/users.model');
const Categories = require('../models/categories.model');


const storage = multer.diskStorage({
    filename: function(req, file, cb) {
        cb(null, new Date().toISOString() + file.originalname);
    },
    destination: function(req, file, cb) {
        cb(null, path.join(__dirname + '/../public/uploads/'));
    }
});

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/jpg') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});
// middleware
// upload.single('myImage')


// authCheck.isAdmin,
// authCheck.isAuthor,

router.get('/', authCheck.isAuthor, (req, res) => {

    Users.find({
            role: 'author'
        })
        .then(users => {
            Categories.find()
                .then(categories => {
                    res.render('admin', {
                        title: '[Admin Page]',
                        categories,
                        users
                    });

                });
        });
});

router.post('/add-category', (req, res) => {
    console.log(req.body);
    const category = new Categories({
        name: req.body.name
    });
    category.save()
        .then(result => {
            res.redirect('/admin');
        });
});

router.post('/add-post', upload.single('myImage'), (req, res) => {

    console.log(req.file);
    req.body.imagePath = req.file.path.split('public')[1];
    const post = new Posts({
        title: req.body.title,
        text: req.body.text,
        author: req.body.author,
        imagePath: req.body.imagePath
    });

    if (req.body.category) {
        post.category = req.body.category;

        Categories.findById(req.body.category)
            .then(category => {
                category.posts.push(post);
                category.save();
            });
    }

    post.save()
        .then(result => {
            Users.findById(req.body.author)
                .then(user => {
                    user.posts.push(result);
                    user.save();
                    res.redirect('/blog');
                })
                .catch(error => console.log(error));
        });

});
// router.post('/add-post', (req, res) => {


//     var form = new formidable.IncomingForm();

//     form.uploadDir = path.join(__dirname + '/../public/uploads/');
//     form.keepExtensions = true;

//     form.on('fileBegin', function(name, file) {
//         file.path = path.join(__dirname + '/../public/uploads/' + new Date().toISOString() + '-' + file.name);
//         req.body.imagePath = file.path.split('public')[1];
//     });

//     form.parse(req, (err, fields, files) => {
//         const post = new Posts({
//             title: fields.title,
//             text: fields.text,
//             author: fields.author,
//             imagePath: req.body.imagePath
//         });

//         if (fields.category) {
//             post.category = fields.category;

//             Categories.findById(fields.category)
//                 .then(category => {
//                     category.posts.push(post);
//                     category.save();
//                 });
//         }

//         post.save()
//             .then(result => {
//                 Users.findById(fields.author)
//                     .then(user => {
//                         user.posts.push(result);
//                         user.save();
//                         res.redirect('/blog');
//                     })
//                     .catch(error => console.log(error));
//             });
//     });


// });

router.get('/get-posts', (req, res) => {
    Posts.find()
        // .populate('comments')
        .populate('category')
        .populate('author')
        .then(posts => {
            res.json(posts);
        });
});

router.get('/get-comments', (req, res) => {
    Comments.find()
        .populate('postID')
        .then(posts => {
            res.json(posts);
        });
});

router.get('/get-users', (req, res) => {
    Users.find()
        .populate('posts')
        .then(posts => {
            res.json(posts);
        });
});
router.get('/get-categories', (req, res) => {
    Categories.find()
        .populate('posts')
        .then(categories => {
            res.json(categories);
        });
});

// router.delete('/delete-post', (req, res) => {
//     console.log(req.body);

//     // Posts.findOne({_id: req.body._id})
//     //     .then(post => {
//     //         if (post) {
//     //             return Posts.remove({
//     //                 _id: req.body._id
//     //             });
//     //         }
//     //         res.send('no such post');
//     //     });
//     Posts.findById({_id: req.body.id})
//         .then(result => {
//             console.log('good');
//         })
//         .catch(error => console.log(error));
// });


module.exports = router;