var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');

const Posts = require('../models/posts.model');
const Users = require('../models/users.model');
const Comments = require('../models/comments.model');
const Categories = require('../models/categories.model');

const authCheck = require('../middleware/check-auth');

const month = {
    month_names: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    month_short_names: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
};

/* GET home page. */
router.get('/', (req, res, next) => {
    // Posts.find()
    //     .sort('-date')
    //     .then(posts => {
    //         res.render('blog', {
    //             title: '[Blog Page]',
    //             posts: posts,
    //             month: month,
    //         });
    //     });
    res.redirect('/blog/1');
    console.log(Users);
});


router.get('/post/:id', (req, res) => {
    const query = {
        _id: req.params.id
    };
    Posts.find(query)
        .populate('comments')
        .populate('category')
        .populate('author')
        .then(result => {
            const post = result[0];
            if (post) {
                post.views += 1;
                res.render('single', {
                    title: post.title,
                    post: post,
                    month: month,
                    user: req.user
                });
                // console.log(post);
                Posts.update(query, post, error => {
                    console.log(error);
                });
            }
        });
});

router.post('/comment/:id', (req, res) => {
    req.checkBody('author', 'Name is required').notEmpty();
    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('email', 'Invalid email').isEmail();
    // Posts.find({_id: req.params.id})
    //     .then(post => {
    //         if (post) {
    //             const comment = new Comments({
    //                 author: req.body.author,
    //                 email: req.body.email,
    //                 text: req.body.text,
    //                 post: req.params.id
    //             });
    //             comment.save();
    //             res.send('Success');
    //         }
    //     });
    const comment = new Comments({
        author: req.user._id,
        email: req.user.email,
        text: req.body.text,
        postID: req.params.id
    });
    comment.save()
        .then(comment => {
            Posts.findById(req.params.id)
                .then(post => {
                    post.comments.push(comment);
                    post.save();
                })
                .catch(error => console.log(error));
            Users.findById(req.user._id)
                .then(user => {
                    user.comments.push(comment);
                    user.save();
                    res.send('Thank you, your comment is sent =)');
                });
        })
        .catch(error => console.log(error));
});

function dynamicSort(property) {
    var sortOrder = 1;
    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function(a, b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    };
}

router.get('/edit/:id', (req, res) => {
    Posts.findById(req.params.id)
        .populate('author')
        .populate('category')
        .then(post => {
            Users.find({
                    role: 'author'
                })
                .then(users => {
                    Categories.find()
                        .then(categories => {
                            res.render('edit', {
                                title: '[Edit Page]',
                                categories,
                                users,
                                post
                            });

                        });
                });

        })
        .catch();
});

router.post('/edit/:id', (req, res) => {
    let post = {};
    post.title = req.body.title;
    post.author = req.body.author;
    post.text = req.body.text;
    post.date = Date.now();

    if (req.body.category) {
        post.category = req.body.category;

        Categories.findById(req.body.category)
            .then(category => {
                category.posts.push(post);
                category.save();
            });
    }

    const query = {
        _id: req.params.id
    };
    Posts.update(query, post, (error) => {
        if (error) {
            console.log(error);
        } else {
            res.redirect('/blog');
        }
    });

});

router.delete('/delete-post/:id', (req, res) => {
    const query = {
        _id: req.params.id
    };

    Posts.findById(req.params.id)
        .then(post => {
            console.log(post);
            fs.unlinkSync(path.join(__dirname + '/../public' + post.imagePath));
        });
    Posts.remove(query, error => {
        if (error) {
            return console.log(error);
        }
        res.send('This post is deleted =(');
    });
});

router.delete('/delete-comment/:id', (req, res) => {
    const query = {
        _id: req.params.id
    };


    Comments.remove(query, error => {
        if (error) {
            return console.log(error);
        }
        res.send('This comment is deleted =(');
    });
});

router.get('/:page', function(req, res, next) {
    var perPage = 3;
    var page = req.params.page || 1;


    Posts.find({})
        .populate('author')
        .populate('category')
        .sort('-date')
        .skip((perPage * page) - perPage)
        .limit(perPage)
        .exec(function(err, posts) {
            Posts.count()
                .sort('-date')
                .exec(function(err, count) {
                    if (err) return next(err);
                    res.render('blog', {
                        current: page,
                        pages: Math.ceil(count / perPage),
                        title: '[Blog Page]',
                        posts: posts.sort(dynamicSort('-date')),
                        month: month,
                    });
                });
        });
});



module.exports = router;
