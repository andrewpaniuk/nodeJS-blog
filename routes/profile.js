var express = require('express');
var router = express.Router();

const Users = require('../models/users.model');



const authCheck = require('../middleware/check-auth');

/* GET profile page. */
router.get('/', authCheck.isLoggedIn, function(req, res, next) {
    res.render('profile', {
        title: '[Profile Page]',
        user: req.user
    });
});

router.get('/:id', authCheck.isLoggedIn, function(req, res, next) {
    Users.findById(req.params.id)
        .then(user => {
            res.render('profile', {
                title: '[Profile Page]',
                user
            });
        });
});

module.exports = router;