module.exports = (req, res, next) => {
    function textHTML(str) {
        var lines = str.split(/\r?\n/);
        var html = '';
        for (var i = 0; i < lines.length; i++) {
            if (lines[i].substr(0, 1) == '#') {
                let line = lines[i].slice(lines[i].indexOf('#') + 1);
                html += `<h3>${line}</h3>`;
                continue;
            }
            html += `<p>${lines[i]}</p>`;
        }
        return html;
    }
    req.textHTML = textHTML;
    next();
};