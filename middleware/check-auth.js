// const jwt = require('jsonwebtoken');

// module.exports = (req, res, next) => {
//     try {
//         const token = req.headers.authorization.split(' ')[1];
//         console.log(token);
//         const decoded = jwt.verify(token, 'andrewisback');
//         req.userData = decoded;
//         next();
//     } catch (error) {
//         return res.status(404).json({
//             message: 'Auth failed'
//         });
//     }
// };

module.exports = {
    isLoggedIn(req, res, next) {
        if (req.isAuthenticated()) {
            return next();
        }
        res.redirect('/');
    },
    notLoggedIn(req, res, next) {
        if (!req.isAuthenticated()) {
            return next();
        }
        res.redirect('/');
    },
    isAdmin(req, res, next) {
        if (req.user && req.user.isAdmin) {
            return next();
        }
        res.redirect('/');
        // console.log(req.user);
    },
    isAuthor(req, res, next) {
        if (req.user && req.user.role === 'author') {
            return next();
        }
        res.redirect('/');
    }
};
