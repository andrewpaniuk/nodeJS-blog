const main_info = {
    main_menu: {
        'home': '/',
        'about': '/about',
        'prices': '/prices',
        'how it works': '/how-it-work',
        'discount': '/discount',
        'order now': '/order',
        'faq': '/faq',
        'testimonials': '/testimonials',
        'blog': '/blog',
        'samples': '/samples',
        'login': '/user/login'
    },
    toll_for_free: '+1-(044)-147-0235',
    call_now: '+1-(044)-147-0235',
    links: {
        pinterest: 'https://pinterest.com',
        twitter: 'https://twitter.com',
        facebook: 'https://fb.com'
    }
};


module.exports = main_info;