const passport = require('passport');
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');

const LocalStrategy = require('passport-local').Strategy;

const Users = require('../models/users.model');

passport.serializeUser((user, done) => {
    done(null, user._id);
});

passport.deserializeUser((id, done) => {
    Users.findById(id, (error, user) => {
        done(error, user);
    });
});

passport.use('local.signup', new LocalStrategy({
    usernameField: 'login',
    passwordField: 'password',
    passReqToCallback: true
}, (req, login, password, done) => {
    req.checkBody('login', 'Email is required').notEmpty();
    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('surname', 'Surname is required').notEmpty();
    req.checkBody('email', 'Invalid email').isEmail();
    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('password', 'Password is required').notEmpty();
    req.checkBody('password', 'Passwords must be at least 8 chars long').isLength({
        min: 8
    });
    req.checkBody('confPassword', 'Passwords do not match.').equals(req.body.password);
    const errors = req.validationErrors();
    if (errors) {
        const messages = [];
        errors.forEach(error => {
            messages.push(error.msg);
        });
        return done(null, false, req.flash('error', messages));
    }
    Users.findOne({
            'login': login
        })
        .then(user => {
            if (user) {
                return done(null, false, {
                    message: 'Login is already in use.'
                });
            }
            Users.findOne({
                    email: req.body.email
                })
                .then(user => {
                    if (user) {
                        return done(null, false, {
                            message: 'Email is already in use.'
                        });
                    }
                    if (req.body.password === req.body.confPassword) {
                        console.log(password, login);
                        const newUser = new Users();
                        newUser.login = login;
                        newUser.password = newUser.encryptPassword(password);
                        newUser.name = req.body.name;
                        newUser.surname = req.body.surname;
                        newUser.email = req.body.email;
                        newUser.save()
                            .then(result => done(null, newUser))
                            .catch(error => done(error));

                        let transporter = nodemailer.createTransport({
                            host: 'smtp.gmail.com',
                            service: 'Gmail',
                            auth: {
                                user: '',
                                pass: ''
                            }
                        });

                        // setup email data with unicode symbols
                        let mailOptions = {
                            from: '"Andrew Paniuk 👻" <foo@example.com>',
                            to: 'awesomedruce@gmail.com',
                            subject: 'Hello ✔',
                            text: 'Hello world?',
                            html: '<b>Hello world?</b>'
                        };

                        // send mail with defined transport object
                        transporter.sendMail(mailOptions, (error, info) => {
                            if (error) {
                                return console.log(error);
                            }
                            console.log('Message sent: %s', info.messageId);
                            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                        });
                    }
                });

        })
        .catch(error => done(error));
}));

passport.use('local.signin', new LocalStrategy({
    usernameField: 'login',
    passwordField: 'password',
    passReqToCallback: true
}, (req, login, password, done) => {
    req.checkBody('login', 'Login is required').notEmpty();
    req.checkBody('password', 'Password is required').notEmpty();
    const errors = req.validationErrors();
    if (errors) {
        const messages = [];
        errors.forEach(error => {
            messages.push(error.msg);
        });
        return done(null, false, req.flash('error', messages));
    }

    Users.findOne({
            login: req.body.login
        })
        .then(user => {
            if (!user) {
                return done(null, false, {
                    message: 'No user found in my DB!'
                });
            }

            if (!user.validPassword(password)) {
                return done(null, false, {
                    message: 'Wrong password, dude!'
                });
            }

            return done(null, user);
        })
        .catch(error => done(error));
}));