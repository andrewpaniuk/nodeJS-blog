var path = require('path');
var createError = require('http-errors');
var express = require('express');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var logger = require('morgan');
var parser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var flash = require('connect-flash');
var validator = require('express-validator');

// Middleware
var textHTML = require('./middleware/text-html');

mongoose.connect('mongoDB://127.0.0.1/ap_')
  .then(() => console.log('mongoDB is connected...'))
  .catch(error => console.log(error));

var app = express();

// Routers
var indexRouter = require('./routes/index');
var userRouter = require('./routes/user');
var blogRouter = require('./routes/blog');
var adminRouter = require('./routes/admin');
var profileRouter = require('./routes/profile');

// main information in all pages (header and footer)
const main_info = require('./db/main-info');

// view engine setup
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(validator());
app.use(cookieParser());
app.use(session({secret: 'Andrew_Is_BaCk', resave: false, saveUninitialized: false}));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));
app.use(parser());
app.use(textHTML);
app.use((req, res, next) => {
    res.locals.login = req.isAuthenticated();
    res.locals.main_info = main_info;
    res.locals.isAdmin = (req.isAuthenticated() && req.user.isAdmin === true) ? true : false;
    res.locals.isAuthor = (req.isAuthenticated() && req.user.role === 'author') ? true : false;
    next();
});

app.use(function(req, res, next) {
    if (req.method == 'POST' && req.url == '/user/login') {
        if (req.body.rememberme) {
            req.session.cookie.maxAge = 2592000000; // 30*24*60*60*1000 Rememeber 'me' for 30 days
        } else {
            req.session.cookie.expires = false;
        }
    }
    next();
});

require('./config/passport');

app.use('/', indexRouter);
app.use('/admin', adminRouter);
app.use('/user', userRouter);
app.use('/blog', blogRouter);
app.use('/profile', profileRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;
