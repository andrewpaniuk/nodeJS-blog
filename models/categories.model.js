var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const categories_Schema = new Schema({
    name: {
        type: String,
        required: true
    },
    posts: [{
        type: Schema.Types.ObjectId,
        ref: 'posts'
    }],
});

module.exports = mongoose.model('categories', categories_Schema);