var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const comments_Schema = new Schema({
    postID: {
        type: Schema.Types.ObjectId,
        ref: 'posts'
    },
    date: {
        type: Date,
        default: Date.now
    },
    author: {
        type: Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },
    text: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('comments', comments_Schema);